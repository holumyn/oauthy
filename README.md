# OAuthy

## OAuth 2.0 API

## Installation instructions

git clone https://gitlab.com/holumyn/oauthy.git

Run `npm install`

Start up your server `npm start`

Run api on `http://127.0.0.1:4000`

Reference: https://expressjs.com/en/starter/installing.html