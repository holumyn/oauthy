const express = require('express'),
  app = express(),
  port = process.env.PORT || 4000;
const url = require('url');
const axios = require('axios');
const creds = require('./api/constants/creds');
const data = require('./api/constants/data');
const bodyParser = require('body-parser');

let JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;
let opts = {}

app.use(bodyParser.json());

app.get('/api/v1', (req, res) => {
  res.status(200).json({message:'Welcome to OAuthy'});
});

app.get('/api/v1/oauth2/authorize/', (req, res) => {

  if(!req.query.client_id){
    res.status(400).json({
      message: 'Bad Request',
    });
  }

  const client_id = req.query.client_id;
  const client_secret = req.query.client_secret;
  const redirect_url = req.query.redirect_url;

  if(client_id !== creds.CLIENT_ID || client_secret !== creds.CLIENT_SECRET){
    res.status(401).json({
      message: 'Unauthorized user',
    });
  }

  if(redirect_url !== creds.REDIRECT_URL){
    res.status(400).json({
      message: 'Unmatched redirect URL',
    });
  }

  const code = creds.FAKE_CODE;

  res.redirect(url.format({
    pathname: redirect_url,
    query: {
      'code': code,
      'client_id': client_id,
      },
    })
  );
});

app.get('/callback', (req, res) => {

  console.log('You may wanna check your state here if set to be sure it`s a valid redirect');
  console.log('Getting access token!!!!');
  axios.get('http://127.0.0.1:4000/api/v1/oauth2/token', {
    params: {
      code: req.query.code,
      client_id: creds.CLIENT_ID
    }
  })
  .then(function (response) {

    console.log(response);
    res.status(200).send(response.data);
  })
  .catch(function (error) {
    console.log(error);
    res.status(400).send(error.data);
  });
});

app.get('/api/v1/oauth2/token', (req, res) => {

  const client_id = req.query.client_id;
  const client_secret = req.query.client_secret;
  const redirect_url = req.query.redirect_url;
  const scope = 'read,write';
  const code = req.query.code;
  const access_token = creds.FAKE_ACCESS_CODE;

  if(code !== creds.FAKE_CODE){
    res.status(401).json({
      message: 'Unauthorized user',
    });
  }

  res.status(200).json({token: access_token});
});

app.get('/api/v1/user', (req, res) => {
  authorize(req, res);
    
  if(req.query === undefined){
    res.status(401).json({
      message: 'Unauthorized Users!!!',
    });
  }

  const sample_data = data.SAMPLE_DATA;
  res.status(200).json({
    data: sample_data,
  });
});

function authorize(req, res){
  if (req.headers && req.headers.authorization 
    && req.headers.authorization.split(' ')[0] === 'Bearer'
    && req.headers.authorization.split(' ')[1] === creds.FAKE_ACCESS_CODE) {
      
      //proceed 
       console.log('Token Valid');
  }else{
    req.query = undefined;
  }
}
app.listen(port);

console.log('OAuthy RESTful API server started on: ' + port);